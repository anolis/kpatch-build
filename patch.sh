KPATCH_VERSION=0.9.9

patches=`ls patches`

tar -xf v$KPATCH_VERSION.tar.gz

for each in $patches; do
	echo "$each"
	patch -d kpatch-$KPATCH_VERSION -p1 < ./patches/$each 
done
