Name:		kpatch
Version:	0.9.9
Release:    1
Summary:	Dynamic kernel patch manager

Group:		System Environment/Kernel
License:	GPLv2
URL:		https://github.com/dynup/kpatch
Source0:	https://github.com/dynup/kpatch/archive/v%{version}.tar.gz

Patch0001:         0001-kpatch-don-t-need-vmlinux.patch
Patch0002:         0002-kpatch-build-fix-some-subfunction-.cold-change-error.patch
Patch0003:         0003-kpatch-get-gcc-version-from-config-file.patch
Patch0004:         0004-get-kernel-version-from-kpatch-build.patch
Patch0005:         0005-kpatch-build-use-new-klp-arch.patch
Patch0006:         0006-check-klp_register_patch-function-symbol.patch
Patch0007:         0007-kpatch-build-set-EXTRAVERSION.patch
Patch0008:         0008-devel-an-options-for-kpatch-module-build-mode.patch
Patch0009:         0009-kpatch-build-add-DISTRO-anolis.patch

Requires:	bash kmod binutils

BuildArch:	noarch


%description
kpatch is a dynamic kernel patch module manager.  It allows the user to manage
a collection of binary kernel patch modules which can be used to dynamically
patch the kernel without rebooting.


%prep
%setup -q
%autosetup -n v%{version} -p1 -Sgit

%build
make -C man


%install
make install PREFIX=/usr DESTDIR=%{buildroot} -C kpatch
make install PREFIX=/usr DESTDIR=%{buildroot} -C man
make install PREFIX=/usr DESTDIR=%{buildroot} -C contrib
rm -f %{buildroot}/usr/share/man/man1/kpatch-build.1.gz
install -m 755 %{SOURCE1} %{buildroot}/usr/sbin/

%files
%{_sbindir}/kpatch
%{_usr}/lib/systemd/system/kpatch.service
%doc %{_mandir}/man1/kpatch.1.gz


%changelog
* Thu Jan 18 2024 zhangyongde <ydjohn@linux.alibaba.com> - 0.9.9-1
- Bump version to 0.9.9

* Fri Sep 6 2019 Joe Lawrence <joe.lawrence@redhat.com> 0.6.1-5
- kpatch: clarify that "kpatch unload" isn't supported (rhbz#1749299)

* Sun Jun 23 2019 Joe Lawrence <joe.lawrence@redhat.com> 0.6.1-3
- Rebuild with correct RHEL-7.7 bugzilla number (rhbz#1719309)

* Sun Jun 23 2019 Joe Lawrence <joe.lawrence@redhat.com> 0.6.1-3
- kpatch script: don't fail if module already loaded+enabled (rhbz#1719309)

* Wed Jun 12 2019 Joe Lawrence <joe.lawrence@redhat.com> 0.6.1-2
- kpatch: patches shouldn't be unloaded on system shutdown (rhbz#1719309)

* Thu Jun 21 2018 Joe Lawrence <joe.lawrence@redhat.com> 0.6.1-1
- update to 0.6.1 (rhbz#1562976)

* Thu Nov 16 2017 Joe Lawrence <joe.lawrence@redhat.com> 0.4.0-3
- kpatch: better livepatch module support (rhbz#1504066)

* Wed Oct 18 2017 Josh Poimboeuf <jpoimboe@redhat.com> 0.4.0-2
- fix backwards compatibility with RHEL 7.3 patches (rhbz#1497735)

* Mon Mar 13 2017 Josh Poimboeuf <jpoimboe@redhat.com> 0.4.0-1
- update to 0.4.0 (rhbz#1427642)

* Wed Jun 15 2016 Josh Poimboeuf <jpoimboe@redhat.com> 0.3.2-1
- update to 0.3.2 (rhbz#1282508)

* Wed Nov 18 2015 Josh Poimboeuf <jpoimboe@redhat.com> 0.3.1-1
- update to 0.3.1 (rhbz#1282508)

* Tue Sep 16 2014 Seth Jennings <sjenning@redhat.com> 0.1.10-4
- fix dracut dependencies (rhbz#1170369)

* Tue Sep 16 2014 Seth Jennings <sjenning@redhat.com> 0.1.10-3
- support re-enabling forced modules (rhbz#1140268)

* Thu Sep 11 2014 Seth Jennings <sjenning@redhat.com> 0.1.10-2
- support modprobe format names (rhbz#1133045)

* Thu Jul 31 2014 Josh Poimboeuf <jpoimboe@redhat.com> 0.1.10-1
- update to kpatch 0.1.10

* Wed Jul 23 2014 Josh Poimboeuf <jpoimboe@redhat.com> 0.1.9-1
- update to kpatch 0.1.9

* Tue Jul 15 2014 Josh Poimboeuf <jpoimboe@redhat.com> 0.1.8-1
- update to kpatch 0.1.8

* Wed May 21 2014 Josh Poimboeuf <jpoimboe@redhat.com> 0.1.2-1
- update to kpatch 0.1.2

* Mon May 19 2014 Josh Poimboeuf <jpoimboe@redhat.com> 0.1.1-2
- fix initramfs core module path

* Mon May 19 2014 Josh Poimboeuf <jpoimboe@redhat.com> 0.1.1-1
- rebase to kpatch 0.1.1

* Fri May 9 2014 Josh Poimboeuf <jpoimboe@redhat.com> 0.1.0-2
- modprobe core module

* Tue May 6 2014 Josh Poimboeuf <jpoimboe@redhat.com> 0.1.0-1
- Initial kpatch release 0.1.0

* Thu Jan 30 2014 Josh Poimboeuf <jpoimboe@redhat.com> 0.0-1
- Initial build
