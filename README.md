# kpatch-build

## 介绍
kpatch-build 是一款热补丁制作工具，可在不重启系统和中断业务的情况下对操作系统内核进行CVE和Bug修复。

支持架构：
X86，ARM64

上游社区：https://github.com/dynup/kpatch

#### 软件架构
软件架构说明
![输入图片说明](image.png)

## 安装教程

教程以龙蜥操作系统Anolis OS 8.6，内核版本4.19.91-26.4.an8.x86_64为例说明热补丁制作全流程。

### 1.安装软件包
```
yum install -y make gcc patch bison flex openssl-devel elfutils elfutils-devel dwarves bc perl
```
在制作热补丁过程中出现命令或头文件找不到，可以根据出错提示安装对应软件包即可。

### 2.下载kernel-debuginfo,kernel-devel,kernel source

Anolis 内核包仓库地址：
[https://mirrors.openanolis.cn/anolis/](https://mirrors.openanolis.cn/anolis/)

以Anolis 8.6为例子：  
源代码下载地址： [https://mirrors.aliyun.com/anolis/8.6/Plus/source/Packages/](https://mirrors.aliyun.com/anolis/8.6/Plus/source/Packages/)  
debuginfo地址：[https://mirrors.aliyun.com/anolis/8.6/Plus/x86_64/debug/Packages/](https://mirrors.aliyun.com/anolis/8.6/Plus/x86_64/debug/Packages/)  
kernel-devel: [https://mirrors.openanolis.cn/anolis/8.6/Experimental/x86_64/os/Packages/](https://mirrors.openanolis.cn/anolis/8.6/Experimental/x86_64/os/Packages/)  

分别解压三个软件包(rpm2cpio xxx.rpm | cpio -div)，依次提取出vmlinux, .config和kernel source，并放置在同一个目录。


### 3.下载kpatch-build
```
wget https://gitee.com/anolis/kpatch-build/repository/archive/master.zip
```

### 4.编译kpatch-build

解压v0.9.9.tar.gz

调用patch.sh脚本，把仓库中的patch打上源码
```
./patch.sh
```
进入kpatch-0.9.9目录下

##### 如果你是需要使用kpatch core module加载(即kpatch.ko)，设置BUILDMOD=yes
```
make BUILDMOD=yes && make BUILDMOD=yes install
```
##### 如果不需要使用kpatch.ko，使用livepatch
```
make && make install
```

### 5.制作热补丁
```
kpatch-build -n kpatch-test  -s /root/hotfix/linux-4.19.91-26.4.an7 -c /root/hotfix/.config -v /root/hotfix/vmlinux -o /root/hotfix/output/ -dddddd -R  /root/hotfix/test-livepatch.patch 
```
其中：
```
-n 补丁名称
-s：指向源代码目录
-c: config文件
-v: vmlinux文件
-o: 产物输出目录
test-livepatch.patch：补丁文件
-d: 输出debug信息
```

#### 5.1 制作oot hotfix
制作oot hotfix的过程与制作kernel hotfix的过程一致，只是输出参数区别
```
kpatch-build -s {path_to_kernel_source} -c {path_to_kernel_config} -v {path_to_kernel_vmlinux}  --oot-module-src {path_to_oot_module_source} --oot-module {path_to_oot_module_ko} {path_to_patch_file}
```

## 使用说明
热补丁中自带kpatch管理工具
加载热补丁：
```
kpatch load kpatch-test.ko
```
卸载热补丁：
```
kpatch unload kpatch-test.ko
```
热补丁列表：
```
kpatch list 
```
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 了解kpatch热补丁
- https://blog.csdn.net/wardenjohn/article/details/132665469?spm=1001.2014.3001.5502 【可以了解kpatch的技术overview】

限制
------
- 注意：使用kpatch-build存在一定的限制，但是其中也有一些规避的方法。详情可以查看[用户指导](doc/patch-author-guide.md)
- 如果你的patch里面是修改了__init函数(由`__init`修饰的函数)的话，这种修改目前kpatch-build也是不支持的。
- 如果你的补丁针对已经静态分配内存的数据的修改，kpatch-build不是直接支持的。当然，kpatch-build中提供了一种shadow变量的方式来进行弥补，详情可见[用户指导](doc/patch-author-guide.md)
- 那些修改了函数与动态分配数据的交互方式的补丁不一定是安全的。kpatch-build不会去验证这一类补丁的安全性。这个完全取决于patch的制作者是不是对其修改有足够的认识，以及对一个正在运行的系统打上这样的补丁会对系统造成什么潜在的影响。
- 修改vdso函数是不支持的，因为这些函数运行在用户态下，并且ftrace hook不了它们。
- 修改那些没有fentry的函数也是不支持的。kpatch-build基于ftrace进行函数跟踪，没有ftrace点将使kpatch无法工作。包括那些为了后续链接的所有链接到`lib.a`的`lib-y`的目标（打个比方：`lib/string.o`）
- 本仓库内的kpatch代码逻辑默认关闭KLP_REPLACE，如果需要请在kpatch-build脚本中打开
